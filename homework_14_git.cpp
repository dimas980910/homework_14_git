﻿#include <iostream>

using namespace std;

int main()
{
    string Word = "Hello";

    cout << "String = " << Word << "\nLength = " << Word.length();
    cout << "\nFirst letter = " << Word.front() << "\nLast letter = " << Word.back() << endl;

    return 0;
}